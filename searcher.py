#!/bin/python -u

import hashlib
import re
import requests
import sqlite3
import sys
import time
import traceback
import urllib.parse
from bs4 import BeautifulSoup
from io import BytesIO
from PIL import Image


def progress(status, remaining, total):
    print(f'Copied {total-remaining} of {total} pages...')

class db_images:
    __name = ':memory:'
    __name_backup = 'images.db'
    __conn = None
    __conn_backup = None
    __urls_to_indexing = 1000
    __backup_timeout = 60
    __backup_last = 0

    def __init__(self):
        self.__conn = sqlite3.connect(self.__name, isolation_level=None)
        self.__conn_backup = sqlite3.connect(self.__name_backup, isolation_level=None)
        self.__conn_backup.execute("""
                CREATE TABLE IF NOT EXISTS config (
                id    text,
                value text
                )""")
        self.__conn_backup.execute("""
            CREATE TABLE IF NOT EXISTS images (
                hash  text PRIMARY KEY,
                url   text,
                attr  text,
                data  text
                )""")
        self.__conn_backup.execute("""
            CREATE TABLE IF NOT EXISTS history (
                hash   text PRIMARY KEY,
                url    text,
                status integer
                )""")
        self.read()
        pass

    def prepare_image(self, image):
        url = image[0]
        attr = image[1]
        data = image[2]
        ret = ['', '', []]

        if (len(url.strip()) <= 0):
            return ret
        if (len(attr.strip()) <= 0):
            return ret

        img = Image.open(BytesIO(data))
        w, h = img.size
        size = 200
        if w < size or h < size:
            return ret
        img = img.convert('RGB')
        m = min(h, w)
        l = (w - m) / 2
        r = l + m
        t = (h - m) / 2
        b = t + m
        img = img.crop((l, t, r, b))
        img = img.resize((size, size), Image.BILINEAR)
        img.save('tmp.jpg')
        pixels = list(img.getdata())
        pixels = [x for xs in pixels for x in xs]

        return [url, attr, pixels]

    def save_image(self, image):
        url = image[0]
        attr = image[1]
        data = image[2]
        data = " ".join([str(i) for i in data])

        hash = hashlib.md5(url.encode('utf-8')).hexdigest()

        if (len(url) > 0):
            self.__conn.execute('INSERT OR REPLACE INTO images (hash, url, attr, data) VALUES (?, ?, ?, ?) ON CONFLICT DO NOTHING', (hash, url, attr, data))
            # print("save image: ", url, attr, data)

        pass

    def save_images(self, images):
        for image in images:
            image = self.prepare_image(image)
            self.save_image(image)

        pass

    def add_url(self, url):
        if (not self.check_history_limit()):
            return

        hash = hashlib.md5(url.encode('utf-8')).hexdigest()

        self.__conn.execute('INSERT OR REPLACE INTO history (hash, url, status) VALUES (?, ?, ?) ON CONFLICT DO NOTHING', (hash, url, 0))
        # print("add url: ", url)

        pass

    def add_urls(self, urls):
        for url in urls:
            self.add_url(url)

        pass

    def get_url(self):
        stmt = self.__conn.execute('SELECT hash, url FROM history WHERE status = 0 LIMIT 1')
        rows = stmt.fetchall()

        if (len(rows) == 0):
            print("ERROR: could not get url")
            return None

        hash = rows[0][0]
        url  = rows[0][1]
        stmt = self.__conn.execute('UPDATE history SET status = 1 WHERE hash = ? AND status = 0', (hash,))
        # print("get url: ", url)
        return url

    def check_history_limit(self):
        stmt = self.__conn.execute('SELECT count(*) FROM history WHERE status = 0')
        rows = stmt.fetchall()

        if (len(rows) == 0):
            return False

        count = rows[0][0]
        # print("count: ", count)

        if (count > self.__urls_to_indexing):
            return False

        return True

    def reset(self):
        self.__conn.execute('UPDATE history SET status = 0')
        pass

    def read(self):
        self.__conn_backup.backup(self.__conn, progress=progress)
        stmt = self.__conn.execute('SELECT id, value FROM config')
        for row in stmt.fetchall():
            if (row[0] == "urls_to_indexing"):
                self.__urls_to_indexing = int(row[1])
            elif (row[0] == "backup_timeout"):
                self.__backup_timeout = int(row[1])
        pass

    def write(self):
        now = time.time()
        if (self.__backup_last + self.__backup_timeout < now):
            self.__backup_last = now
            self.__conn.execute('DELETE FROM config')
            self.__conn.execute('INSERT INTO config (id, value) VALUES (?, ?)',
                    ("urls_to_indexing", self.__urls_to_indexing))
            self.__conn.execute('INSERT INTO config (id, value) VALUES (?, ?)',
                    ("backup_timeout", self.__backup_timeout))
            self.__conn.backup(self.__conn_backup, pages=100000, progress=progress)
        pass



class html_handler:
    def parse_html(self, url):
        try:
            response = requests.get(url, timeout=10)
        except:
            print(url)
            print("Error: requests.get(url) exception")
            return [[], []]

        code = response.content
        soup = BeautifulSoup(code, 'html.parser')
        images = [] 
        urls = []

        for link in soup.find_all('img'):
            src = link.get('src') if link.get('src') else ''
            if not src:
                continue
            src = self.abs_url(url, src)
            attr = ''
            if link.get('alt'):
                attr = attr + ' ' + str(link.get('alt'))
            if link.get('title'):
                attr = attr + ' ' + str(link.get('title'))
            if (link.parent.name == 'a' and link.parent.get('title')):
                attr = attr + ' ' + str(link.parent.get('title'))

            attr = re.findall(r'\w+', str(attr))
            attr = " ".join(re.findall(r'\w+', str(attr)))

            data = self.get_image_data(src)

            image = [src, attr, data]
            images.append(image)
            # print(image)

        for link in soup.find_all('a'):
            href = link.get('href')
            if not href:
                continue
            url_href = self.abs_url(url, href)
            urls.append(url_href)
            # print("href: ", url_href)

        return [images, urls]

    def get_image_data(self, url):
        if (not url):
            return bytes()
        try:
            response = requests.get(url, timeout=10)
        except:
            print(url)
            print("Error: requests.get(image) exception")
            return bytes()
        return response.content

    def abs_url(self, url_orig, url):
        url_parsed = urllib.parse.urlparse(url)
        # print(url_parsed)
        if (not url_parsed.netloc):
            url_parsed = url_parsed._replace(netloc=urllib.parse.urlparse(url_orig).netloc)
        if (not url_parsed.scheme):
            url_parsed = url_parsed._replace(scheme=urllib.parse.urlparse(url_orig).scheme)
        if (url_parsed.scheme != 'http' and url_parsed.scheme != 'https'):
            return ''
        return url_parsed.geturl()



class searcher_images:
    def __init__(self):
        self.__db_images = db_images()
        self.__html_handler = html_handler()

    def run(self):
        # self.__db_images.reset()
        # self.__db_images.add_url('https://search.yahoo.com/search?p=sun&fr=yfp-t&fp=1&toggle=1&cop=mss&ei=UTF-8');
        # self.__db_images.add_url('https://www.google.com/search?q=sun');
        # self.__db_images.add_url('https://yandex.ru/search/?text=sun');
        # self.__db_images.add_url('https://riversideparknyc.org/event/sun-gaze-days-2017/?instance_id=6295');
        self.__db_images.add_url('https://goodfon.ru');
        # self.__db_images.add_url('https://unsplash.com');
        while (True):
            # print("time: ", time.ctime())
            try:
                url = self.__db_images.get_url()
                arr = self.__html_handler.parse_html(url)
                images = arr[0]
                urls = arr[1]
                self.__db_images.save_images(images)
                self.__db_images.add_urls(urls)
                self.__db_images.write()
            except:
                print("Error: Oops! exception")
                traceback.print_exc(file=sys.stdout)
                pass
            time.sleep(0.01)
        pass



searcher = searcher_images()
searcher.run()



